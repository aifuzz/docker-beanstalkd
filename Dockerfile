FROM alpine:3.10.3

RUN apk add --no-cache beanstalkd

# Create a group and user
RUN addgroup -S beanstalkd && adduser -S beanstalkd -G beanstalkd

## Beanstalkd binlog setting
RUN mkdir -p /beanstalkd/binlog
RUN chown -R beanstalkd:beanstalkd /beanstalkd

USER beanstalkd

EXPOSE 11300

CMD beanstalkd -p 11300 -u beanstalkd -b /beanstalkd/binlog -V