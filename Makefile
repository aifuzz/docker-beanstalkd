REPOSITORY_URL=registry.gitlab.com/skycru/docker-beanstalkd
BUILD_VERSION=`git rev-parse --short=8 HEAD`

default:

build:
	REPOSITORY_URL=${REPOSITORY_URL} BUILD_VERSION=${BUILD_VERSION} \
	docker-compose build

shell:
	REPOSITORY_URL=${REPOSITORY_URL} BUILD_VERSION=${BUILD_VERSION} \
	docker-compose run --rm build bash

push:
	REPOSITORY_URL=${REPOSITORY_URL} BUILD_VERSION=${BUILD_VERSION} \
	docker-compose push

up:
	REPOSITORY_URL=${REPOSITORY_URL} BUILD_VERSION=${BUILD_VERSION} \
	docker-compose up
